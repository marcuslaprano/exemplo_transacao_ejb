package br.example.session.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import br.example.dao.ProdutoDao;
import br.example.entity.Produto;
import br.example.enuns.StatusAvaliacao;
import br.example.exception.ValidacaoException;
import br.example.session.ProdutoSession;

/**
 * Classe de teste criada como exemplo de conhecimento.
 * 
 * O objeto DAO esta sendo mockado e injetado diretamente no session.
 * Os retornos estão sendo mockados.
 * 
 * @author marcusmazzo
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ProdutoSessionTest {
	
 	@Mock private ProdutoDao dao = new ProdutoDao();
 	@InjectMocks private ProdutoSession session = new ProdutoSession();
 	
 	@Before
 	public void iniciar(){
 		MockitoAnnotations.initMocks(this);
 	}
 	
 	@Test
 	public void testSucessAtualizarSemErro(){
 		Produto ap = new Produto();
 		ap.setId(1L);
 		
 		Mockito.when(dao.findById(ap)).thenReturn(retorno());
 		Mockito.when(dao.update(ap)).thenReturn(retornoUpdate());
 		
 		try {
			Produto produto = session.atualizarSemErro(ap);
			Assert.assertEquals(produto.getStatus(), StatusAvaliacao.AVALIACAO_INICIADA);
		} catch (ValidacaoException e) {
			e.printStackTrace();
			Assert.fail();
		}
 	}

	private Produto retornoUpdate() {
		Produto ap = retorno();
		ap.setStatus(StatusAvaliacao.AVALIACAO_INICIADA);
		return ap;
	}

	private Produto retorno() {
		Produto ap = new Produto();
		ap.setId(1L);
		ap.setStatus(StatusAvaliacao.SEM_AVALIACAO);
		ap.setDescricao("Produto 1");
		return ap;
	}

}
