package br.example.dao.test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.example.dao.ProdutoDao;
import br.example.entity.Produto;


/**
 * Para a execução correta dos testes é necessário que o servidor esteja em execução para poder localizar o
 * datasource.
 * 
 * O database poderia ser mockado utilizando DBUnit.
 * 
 * Teste criado apenas como exemplo para demonstrar o conhecimento.
 * 
 * @author Marcus Mazzo Laprano
 *
 */
public class ProdutoDaoTest {
	
	private ProdutoDao dao = new ProdutoDao();
	
	@Before
	public void configurarEntityManager(){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("bnb");
		dao.setEntityManager(emf.createEntityManager());
	}
	
	@Test
	public void testSucessLocalizarPorDescricao(){
		Produto produto = new Produto();
		produto.setDescricao("Produto 1");
		
		Assert.assertNotNull(dao.localizarProdutoPorDescricao(produto));
	}
	
}
