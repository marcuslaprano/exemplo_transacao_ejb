package br.example.dao.test;

import java.math.BigDecimal;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.example.dao.AvaliacaoProdutoDao;
import br.example.entity.AvaliacaoProduto;
import br.example.entity.Produto;


/**
 * Para a execução correta dos testes é necessário que o servidor esteja em execução para poder localizar o
 * datasource.
 * 
 * O database poderia ser mockado utilizando DBUnit.
 * 
 * Teste criado apenas como exemplo para demonstrar o conhecimento.
 * 
 * @author Marcus Mazzo Laprano
 *
 */
public class AvaliacaoProdutoDaoTest {
	
	private AvaliacaoProdutoDao dao = new AvaliacaoProdutoDao();
	
	@Before
	public void configurarEntityManager(){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("bnb");
		dao.setEntityManager(emf.createEntityManager());
	}
	
	@Test
	public void testSucessLocalizarAvaliacaoPorProduto(){
		AvaliacaoProduto ap = new AvaliacaoProduto();
		ap.setProduto(new Produto());
		ap.getProduto().setId(1L);
		BigDecimal valor = dao.localizarAvaliacaoPorProduto(ap);
		Assert.assertNotNull(valor);
	}
	
	@Test
	public void testSucessFindById(){
		AvaliacaoProduto ap = new AvaliacaoProduto();
		ap.setId(1L);
		ap = dao.findById(ap);
		
		Assert.assertNotNull(ap);
		Assert.assertEquals(ap.getProduto().getDescricao(), "Produto 1");
		
	}

}
