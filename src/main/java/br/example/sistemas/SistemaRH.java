package br.example.sistemas;

import java.util.Observable;
import java.util.logging.Logger;

import javax.ejb.Stateless;

import br.example.entity.AvaliacaoProduto;
import br.example.observable.ISistemaObserver;
import br.example.observable.SistemaObservable;

@Stateless
public class SistemaRH implements ISistemaObserver{
	
	private static final Logger logger = Logger.getLogger(SistemaRH.class.getName());

	@Override
	public void update(Observable sistema, Object avaliacao) {
		if(sistema instanceof SistemaObservable){
			doSomething((AvaliacaoProduto)avaliacao);
		}
	}

	public void doSomething(AvaliacaoProduto avaliacao) {
		logger.info("A avaliacao foi: "+avaliacao.getProduto().getStatus().name()+" com nota: "+avaliacao.getIndiceAvaliacao());
	}

}
