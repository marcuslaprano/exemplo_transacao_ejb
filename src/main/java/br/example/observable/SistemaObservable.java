package br.example.observable;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.example.entity.AvaliacaoProduto;
import br.example.session.AvaliacaoSession;
import br.example.sistemas.SistemaFinanceiro;
import br.example.sistemas.SistemaRH;

/**
 * Classe observable utilizada para implementar a regra 1.
 * A finalidade desta classe é registrar 
 * @author marcusmazzo
 *
 */
@Stateless
public class SistemaObservable extends Observable{

	private Logger logger = Logger.getLogger(SistemaObservable.class.getName());
	
	private List<Observer> sistemas = new ArrayList<>();
	
	@EJB private AvaliacaoSession avaliacaoSession;

	public void registrarListener() {
		SistemaRH sistema1 = new SistemaRH();
		SistemaFinanceiro sistema2 = new SistemaFinanceiro();
        sistemas.add(sistema1);
        sistemas.add(sistema2);
	}
	
	
	public void notifyObservers(AvaliacaoProduto avaliacaoProduto) {
		for(Observer observer: sistemas){
			logger.info("Observador: "+observer.getClass());
			observer.update(this, avaliacaoProduto);
		}
		sistemas.clear();
	}
}
