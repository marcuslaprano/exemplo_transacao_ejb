package br.example.observable;

import java.util.Observer;

import br.example.entity.AvaliacaoProduto;

public interface ISistemaObserver extends Observer{
	
	void doSomething(AvaliacaoProduto avaliacao);

}
