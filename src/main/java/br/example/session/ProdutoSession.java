package br.example.session;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.example.dao.ProdutoDao;
import br.example.entity.Produto;
import br.example.enuns.StatusAvaliacao;
import br.example.exception.ValidacaoException;


/**
 * Classe responsável por simular o isolamento de transação.
 * 
 * Em cada método é realizado chamada para findById apenas para localizar o registro diretamente.
 * Em ambiente de produção esta informação deverá vir preenchida de uma tela ou outro session bean, não sendo
 * necessário a execução desta chamada.
 * 
 * 
 * @author marcusmazzo
 *
 */
@Stateless
public class ProdutoSession {
	
	private static final Logger logger = Logger.getLogger(ProdutoSession.class.getName());
	
	@EJB private ProdutoDao dao;
	
	public ProdutoSession(ProdutoDao dao) {
		super();
		this.dao = dao;
	}
	
	public ProdutoSession() {
		super();
	}


	/**
	 * Método que lança Runtime para simular o erro de banco.
	 * Ao lançar {@link EJBException} a transação é cancelada
	 * e os dados não são comitados no banco.
	 * @param produto
	 * @throws ValidacaoException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void atualizar(Produto produto) throws ValidacaoException {
		Produto ap = findById(produto);
		logger.info("Entrada atualizar inicio: "+ap.getStatus().name());
		ap.setStatus(StatusAvaliacao.POSITIVO);
		dao.update(ap);
		logger.info("Saída Atualizar (Requires news): "+ap.getStatus().name());
		throw new EJBException("Simulando Erro");
	}

	/**
	 * Método que propaga a transação do método pai.
	 * @param produto
	 * @throws ValidacaoException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executarUpdate(Produto produto) throws ValidacaoException {
		Produto ap = findById(produto);
		logger.info("Entrada executarUpdate inicio: "+ap.getStatus().name());
		ap.setStatus(StatusAvaliacao.NEGATIVO);
		dao.update(ap);
		logger.info("Saída Executar Update (Required): "+ap.getStatus().name());
	}

	/**
	 * Ao finalizar o método os dados são comitados.
	 * @param avaliacao
	 * @throws ValidacaoException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Produto atualizarSemErro(Produto avaliacao) throws ValidacaoException {
		Produto ap = findById(avaliacao);
		logger.info("Entrada atualizarSemErro inicio: "+ap.getStatus().name());
		ap.setStatus(StatusAvaliacao.AVALIACAO_INICIADA);
		dao.update(ap);
		logger.info("Saída Atualizar Sem Erro (Requires news): "+ap.getStatus().name());
		return ap;
	}
	
	public Produto findById(Produto produto) throws ValidacaoException {
		validar(produto);
		return dao.findById(produto);
	}
	
	public List<Produto> localizarPorDescricao(Produto produto) throws ValidacaoException {
		if(produto == null){
			throw new ValidacaoException("Objeto nulo");
		}
		
		if(produto.getDescricao() == null || produto.getDescricao().isEmpty()){
			throw new ValidacaoException("Descrição nula");
		}
		
		return dao.localizarProdutoPorDescricao(produto);
	}
	
	private void validar(Produto Produto) throws ValidacaoException{
		if(Produto == null){
			throw new ValidacaoException("Objeto Nulo");
		}
		
		if(Produto.getId() == null){
			throw new ValidacaoException("ID Nulo");
		}
	}

}
