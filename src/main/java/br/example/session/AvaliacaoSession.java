package br.example.session;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.example.dao.AvaliacaoProdutoDao;
import br.example.entity.AvaliacaoProduto;
import br.example.enuns.StatusAvaliacao;
import br.example.exception.ValidacaoException;
import br.example.principal.Principal;
import br.example.util.Util;

@Stateless
public class AvaliacaoSession implements Serializable{

	private static final long serialVersionUID = -7753108689806860212L;
	
	private static final Logger logger = Logger.getLogger(AvaliacaoSession.class.getName());
	
	@EJB private AvaliacaoProdutoDao dao;
	@EJB private Util util;
	@EJB private ProdutoSession produtoSession;
	
	/** Construtor utilizado para realização de testes.
	 * 
	 * @param dao
	 */
	public AvaliacaoSession(AvaliacaoProdutoDao dao) {
		super();
		this.dao = dao;
	}
	
	public AvaliacaoSession() {
		super();
	}

	// INICIO REGRA 1
	public void avaliar(AvaliacaoProduto avaliacaoProduto) {
		try {
			executarAvaliacao(avaliacaoProduto);
		} catch (ValidacaoException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método responsável por executar a avaliação do produto.
	 * Será criado uma nova avaliação a cada execucação.
	 * 
	 * @param avaliacaoProduto
	 * @return
	 * @throws ValidacaoException
	 */
	public AvaliacaoProduto executarAvaliacao(AvaliacaoProduto avaliacaoProduto) throws ValidacaoException {
		avaliacaoProduto = findById(avaliacaoProduto);
		validarProdutoAvaliacao(avaliacaoProduto);

		criarAvaliacao(avaliacaoProduto);

		avaliacaoProduto.setIndiceAvaliacao(dao.localizarAvaliacaoPorProduto(avaliacaoProduto));
		return avaliacaoProduto;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private AvaliacaoProduto criarAvaliacao(AvaliacaoProduto avaliacaoProduto) {
		AvaliacaoProduto avaliacao = new AvaliacaoProduto();
		avaliacao.setProduto(avaliacaoProduto.getProduto());
		avaliacao.getProduto().setStatus(StatusAvaliacao.AVALIACAO_INICIADA);
		avaliacao.setIndiceAvaliacao(util.randomizarAvaliacao());
		dao.insert(avaliacao);
		return avaliacao;
	}

	// FIM REGRA 1
	

	// INICIO REGRA 2 - Explicação sobre como o isolamento funciona
	/**
	 * Método que simula o isolamento das transações.
	 * A chamada ao método atualizar irá lançar uma exceção unchecked para simular um rollback de banco.
	 *
	 * A chamada ao método atualizarSemErro não irá lançar exceção e irá realizar o commit ao término dele.
	 * 
	 * A chamada ao método executarUpdate não irá lançar exceção e irá realizar o commit ao término do método principal.
	 * 
	 * Na primeira chamada, ao lançar uma exceção não checada, a transação será revertida e o commit desfeito, garantindo
	 * a atomicidade do processo. Isso não irá influenciar na transação do método pai pois as transações estão isoladas.
	 * 
	 * Para que as transações sejam isoladas faz-se necessário que as chamadas sejam feitas entre sessions beans
	 * distintos. Caso a chamada seja feita interna a um mesmo session bean a transação somente será comitada ao final do
	 * método principal, ou seja, o método que iniciou o procedimento.
	 * 
	 * Exemplo:
	 * 		Transação A - Required 		- Session Bean AvaliacaoSession
	 * 		Transação B - Requires New	- Session Bean ProdutoSession
	 * 
	 * A transação B é independente da Transação A
	 * 
	 * 		Transação A - Required 		- Session Bean AvaliacaoSession
	 * 		Transação B - Requires New	- Session Bean AvaliacaoSession
	 * 
	 * A transação B é dependente da Transação A
	 * 
	 * 
	 * Verifique que mesmo a exceção sendo lançada posterior ao update (required) o mesmo será comitado ao final do processo.
	 * 
	 * O processo inicia com status = SEM_AVALIACAO, Passando a ter valor NEGATIVO.
	 * 
	 * Logo após é alterado o valor para POSITIVO sendo que esta chamada simula uma exceção de banco ({@link RuntimeException})
	 * com isso o valor não é comitado.
	 * 
	 * Logo após isso o valor é alterado para AVALICAO_INICIADA e com a saida do método a transação será comitada e o valor persistido no 
	 * banco de dados.
	 * 
	 * Ao final do processo (iniciado na classe {@link Principal}) a transação será comitada, alterando o valor de AVALIACAO_INICIADA
	 * para NEGATIVO.
	 * 
	 * Para visualizar o processo ocorrendo colocar breakpoints e validar os valores no banco de dados, passo a passo.
	 * 
	 * @param avaliacao
	 * @throws ValidacaoException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void simularIsolamento(AvaliacaoProduto avaliacao) throws ValidacaoException {
		logger.info("==============================================================================");
		logger.info("Iniciando simulação de isolamento");
		produtoSession.executarUpdate(avaliacao.getProduto());
		try {
			produtoSession.atualizar(avaliacao.getProduto());
		} catch (Exception e) {
			logger.severe(e.getMessage());
		}
		produtoSession.atualizarSemErro(avaliacao.getProduto());
		logger.info("Finalizando simulação de isolamento");
		logger.info("==============================================================================");
	}
	// FIM REGRA 2

	// MÉTODOS AUXILIARES
	public AvaliacaoProduto findById(AvaliacaoProduto avaliacao) throws ValidacaoException {
		validar(avaliacao);
		return dao.findById(avaliacao);
	}

	private void validar(AvaliacaoProduto avaliacaoProduto) throws ValidacaoException{
		if(avaliacaoProduto == null){
			throw new ValidacaoException("Objeto Nulo");
		}
		
		if(avaliacaoProduto.getId() == null){
			throw new ValidacaoException("ID Nulo");
		}
	}

	private void validarProdutoAvaliacao(AvaliacaoProduto avaliacaoProduto)
			throws ValidacaoException {
		if(avaliacaoProduto == null){
			throw new ValidacaoException("Objeto Nulo");
		}
		
		if(avaliacaoProduto.getProduto() == null){
			throw new ValidacaoException("Produto nulo");
		}
		
		if(avaliacaoProduto.getProduto().getId() == null){
			throw new ValidacaoException("Produto ID nulo");
		}
	}
}
