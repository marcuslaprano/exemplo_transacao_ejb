package br.example.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.example.entity.Produto;

@Stateless
public class ProdutoDao extends GenericDao<Produto> implements Serializable{

	private static final long serialVersionUID = 7910143502039652023L;

	public Produto findById(Produto produto){
		produto = getEntityManager().find(produto.getClass(), produto.getId());
		getEntityManager().detach(produto);
		return produto;
	}
	
	public List<Produto> localizarProdutoPorDescricao(Produto produto){
		TypedQuery<Produto> query = getEntityManager().createNamedQuery("Produto.localizarPorDescricao", Produto.class);
		query.setParameter("descricao", "%"+produto.getDescricao()+"%");
		return query.getResultList();
	}
	
}
