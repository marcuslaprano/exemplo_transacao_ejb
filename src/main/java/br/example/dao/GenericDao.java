package br.example.dao;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GenericDao<T> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(name="bnb", unitName="bnb")
	private EntityManager em;
	
	protected EntityManager getEntityManager() {
		if (em == null) {
			throw new IllegalStateException("entityManger null");
		}
		return em;
	}
	
	public T insert(T t){
		em.persist(t);
		return t;
	}
	
	public T update(T t){
		em.merge(t);
		em.detach(t);
		return t;
	}
	
	public void delete(T t){
		em.remove(em.merge(t));
	}
	
	public void setEntityManager(EntityManager em){
		this.em = em;
	}
}
