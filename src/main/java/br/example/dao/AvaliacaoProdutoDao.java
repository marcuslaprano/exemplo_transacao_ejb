package br.example.dao;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import br.example.entity.AvaliacaoProduto;
import br.example.entity.Produto;

@Stateless
public class AvaliacaoProdutoDao extends GenericDao<AvaliacaoProduto> implements Serializable{

	private static final long serialVersionUID = 7910143502039652023L;
	

	/**
	 * Este método localiza o somatório de avaliações de um determinado produto.
	 * @param avaliacaoProduto
	 * @return
	 */
	public BigDecimal localizarAvaliacaoPorProduto(AvaliacaoProduto avaliacaoProduto) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<BigDecimal> query = cb.createQuery(BigDecimal.class);
		Root<AvaliacaoProduto> root = query.from(AvaliacaoProduto.class);

		Join<AvaliacaoProduto, Produto> produto = root.join("produto");
		query.where(cb.equal(produto.get("id"), cb.parameter(String.class, "id")));
		CriteriaQuery<BigDecimal> select = query.select(cb.sum(root.<BigDecimal>get("indiceAvaliacao")));
		
		TypedQuery<BigDecimal> type = getEntityManager().createQuery(select);
		type.setParameter("id", avaliacaoProduto.getProduto().getId());
		
		BigDecimal avaliacao = null;
		try {
			avaliacao = type.getSingleResult();
		} catch (Exception e) {
			avaliacao = new BigDecimal("0");
		}
		return avaliacao;
	}

	public AvaliacaoProduto findById(AvaliacaoProduto avaliacao){
		avaliacao = getEntityManager().find(avaliacao.getClass(), avaliacao.getId());
		getEntityManager().detach(avaliacao);
		return avaliacao;
	}
	
}
