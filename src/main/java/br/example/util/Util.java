package br.example.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.concurrent.ThreadLocalRandom;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import br.example.entity.AvaliacaoProduto;
import br.example.exception.ValidacaoException;
import br.example.session.AvaliacaoSession;


/**
 * Classe {@link Singleton} que simula a entrada de dados de um usuário o sistema.
 * @author marcusmazzo
 *
 */
@Singleton
public class Util {
	
	@EJB private AvaliacaoSession session;
	
	/**
	 * Método que cria uma avaliação.
	 * @param id
	 * @return
	 */
	public AvaliacaoProduto criarAvaliacao(Long id) {
		AvaliacaoProduto avaliacao = new AvaliacaoProduto();
		avaliacao.setId(id);
		try {
			avaliacao = session.findById(avaliacao);
		} catch (ValidacaoException e) {
			e.printStackTrace();
		}
		return avaliacao;
	}

	/**
	 * Método que simula uma avaliação randomizando valores.
	 * @return
	 */
	public BigDecimal randomizarAvaliacao() {
		BigDecimal avaliacao = new BigDecimal("0");
		Double valor = ThreadLocalRandom.current().nextDouble(1, 5);
		NumberFormat nb = NumberFormat.getInstance();
		nb.setMaximumFractionDigits(2);
		nb.setMinimumFractionDigits(2);
		try {
			avaliacao = new BigDecimal(nb.parse(nb.format(valor)).toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return avaliacao;
	}
	}
