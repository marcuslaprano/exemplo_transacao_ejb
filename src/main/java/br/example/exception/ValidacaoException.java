package br.example.exception;

public class ValidacaoException extends Exception {

	private static final long serialVersionUID = -34287186111385866L;
	
	public ValidacaoException(String message) {
		super(message);
	}

}
