package br.example.enuns;

public enum StatusAvaliacao {
	
	SEM_AVALIACAO,AVALIACAO_INICIADA, POSITIVO, NEGATIVO;

}
