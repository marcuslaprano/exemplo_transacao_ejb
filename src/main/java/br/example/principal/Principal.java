package br.example.principal;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.example.entity.AvaliacaoProduto;
import br.example.exception.ValidacaoException;
import br.example.observable.SistemaObservable;
import br.example.session.AvaliacaoSession;
import br.example.util.Util;


/**
 * Classe criada para simular entradas do sistema. Foi utilizado um {@link Schedule} do EJB.
 * Este {@link Schedule} esta sendo executado a cada 50 segundos
 * 
 * @author marcusmazzo
 */
@Stateless
public class Principal {
	
	@EJB private SistemaObservable business;
	@EJB private AvaliacaoSession avaliacaoSession;
	@EJB private Util util;
	
	@Schedule(hour="*", minute="*", second="*/50", persistent=false)
	public void iniciar(){
		regra1(util.criarAvaliacao(1L));
		regra2(util.criarAvaliacao(2L));
	}

	/**
	 * Simular a questão 1 proposta.
	 * 
	 * Foi utilizado, como estratégia, um pattern Observer para registrar os sistemas 
	 * interessados e avisá-los ao término do processamento.
	 * 
	 * O EJB {@link SistemaObservable} é responsável por registrar e comunicar os observadores (listeners)
	 * registrados do resultado do processamento.
	 * 
	 * Foi criado, dentro do mesmo projeto, os listeners. Os mesmos encontram-se 
	 * no pacote "br.example.sistemas". Em um ambiente real poderia ser criado uma interface
	 * onde os sitemas interessados poderiam implementá-la e serem registrados nos processos desejados.
	 * 
	 * @param avaliacao
	 */
	private void regra1(AvaliacaoProduto avaliacao) {
		try {
			business.registrarListener();
			avaliacao = avaliacaoSession.executarAvaliacao(avaliacao);
			business.notifyObservers(avaliacao);
		} catch (ValidacaoException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Simular a questão 2 proposta
	 * 
	 * Para o isolamento funcionar corretamente, devido ao tipo de transação utilizada (JTA) é necessário
	 * no pool de conexão habilitar as conexões não transacionais, setar o tipo de isolação transacional
	 * (read committed) e habilitar que todas as transações possuam o mesmo nível de isolação.
	 * 
	 * Para transações de tipo RESOURCE_LOCAL é possível setar o isolamento diretamente na conexão.
	 * 
	 * 
	 * @param avaliacao
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void regra2(AvaliacaoProduto avaliacao) {
		try {
			avaliacaoSession.simularIsolamento(avaliacao);
		} catch (ValidacaoException e) {
			e.printStackTrace();
		}
	}
}
