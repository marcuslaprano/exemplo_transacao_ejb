package br.example.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="avaliacao_produto", schema="produto")
public class AvaliacaoProduto implements Serializable{

	private static final long serialVersionUID = -2430699695981621161L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name="produto_id", nullable = false)
	private Produto produto = new Produto();
	
	private BigDecimal indiceAvaliacao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getIndiceAvaliacao() {
		return indiceAvaliacao;
	}

	public void setIndiceAvaliacao(BigDecimal indiceAvaliacao) {
		this.indiceAvaliacao = indiceAvaliacao;
	}
}
